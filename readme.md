## Build

To build the mini-site run

	npm install
	gulp

You can now open index.html in the `./dist` folder.
